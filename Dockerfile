ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_python:3.8.2 AS opt_python

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_tools:3.11.3 AS build

COPY --from=opt_python /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV PKG_CONFIG_PATH /opt/pkgconfig/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/pyega3-3.0.44.tar.gz || \
                       wget -t 3 https://files.pythonhosted.org/packages/48/ce/0ab9596a1a0ecc294e248b66eba85d913403f714ebd4aa6a1e17d2658262/pyega3-3.0.44.tar.gz && \
    cd /tmp/scratch/ && tar -xvf pyega3-3.0.44.tar.gz && \
    cd /tmp/scratch/pyega3-3.0.44/ && python -m pip install ./ && \
    cd / && rm -rf /tmp/scratch/

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=build /opt/bin/ /opt/bin/
COPY --from=build /opt/lib/ /opt/lib/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

ENTRYPOINT [ "/opt/bin/pyega3" ]
